﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class Main : MonoBehaviour
{
    static public Main S;

    [Header("Set in Inspector")]
    public GameObject prefabEnemies;
    public int PosEnemyY = 30;
    public int PosEnemyX = -16;
    public int step = 8;
    public int minEnemyShoot = 1;
    public int maxEnemyShoot = 8;

    [SerializeField]
    Transform pointPrefab = default;

    private void Start()
    {
        SpawnEnemy();
    }

    private void SpawnEnemy()
    {
        for (int i = 0; i < 6; i++)
        {
            Vector3 posI = new Vector3(0, 14, 0);
            for (int j = 0; j < 4; j++)
            {
                GameObject enemyGO = Instantiate<GameObject>(prefabEnemies);
                posI.x = PosEnemyX + (step * i);
                posI.y += PosEnemyY * 0.2f;
                enemyGO.transform.position = posI;
            }
        }
    }

   
    private void Awake()
    {
        var position = Vector3.zero;
    }

    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("_Scene_0");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
