﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : MonoBehaviour
{
    public float waveFrequency = 4;
    public float waveWidth = 4;
    public float waveRotY = 45;

    private float x0;
    private float birthTime;

    public int enemyHP;
    public GameObject bullet;
    public float shootDelay;
    public Transform shootPoint;
    public Hero hero;
    public bool canShoot = true;

    private BoundsCheck bndCheck;

    private void Awake()
    {
        bndCheck = GetComponent<BoundsCheck>();
    }
    private void Start()
    {
        x0 = pos.x;
        Shoot();
        hero = GameObject.Find("_Hero").GetComponent<Hero>();
        
    }
    public void Move()
    {
        Vector3 tempPos = pos;
        float age = Time.time - birthTime;
        float theta = Mathf.PI * 2 * age / waveFrequency;
        float sin = Mathf.Sin(theta);
        tempPos.x = x0 + waveWidth * sin;
        pos = tempPos;

        Vector3 rot = new Vector3(0, sin * waveRotY, 0);
        this.transform.rotation = Quaternion.Euler(rot);
    }
    public Vector3 pos
    {
        get
        {
            return (this.transform.position);
        }
        set
        {
            this.transform.position = value;
        }
    }
    private void Update()
    {
        Move();

        if(enemyHP == 0)
        {
            Destroy(gameObject);
            hero.AddScore(100);
        }
        else if(bndCheck.offDown)
        {
            hero.Damage(1);
            Destroy(gameObject);
            hero.AddScore(100);
        }
    }
    public void Shoot()
    {
        StartCoroutine(ShootBetwenShoot());
    }
    
    public void Damage(int dmg)
    {
        enemyHP -= dmg;
        if (enemyHP < 0)
        {
            enemyHP = 0;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ProjectileHero"))
        {
            Damage(hero.bulletDamage);
            Destroy(collision.gameObject);
        }
    }
    
    public IEnumerator ShootBetwenShoot()
    {
        yield return new WaitForSeconds(3);
        if (canShoot == true)
        {
            GameObject enemyBullet = Instantiate(bullet, shootPoint.position, Quaternion.identity) as GameObject;
        }
        StartCoroutine(ShootBetwenShoot());
    }
}
