﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public float speed;
    public Vector2 moveDir;
    void Update()
    {
        transform.Translate(moveDir * Time.deltaTime * speed);
    }
}
