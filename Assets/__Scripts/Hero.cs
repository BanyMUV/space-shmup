﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hero : MonoBehaviour
{

    [Header("Set in Inspector")]
    public float speed = 30;
    public float gameRestartDelay = 2f;
    public float rollMult = -30;
    public float shootDelay;

    public int bulletDamage;
    public int heroHP = 5;
    public int scoreCount;

    public bool isFire;
    public bool isReadyToShoot;
   
    public Text scoreCountText;

    public Transform[] shootPoints;
    public Image[] lifePoints;
    public Color[] lifeColors;

    public GameObject projectilePferab;
    public GameObject restartMenuUI;
    public GameObject lastTriggerGo = null;

   
    private void Start()
    {
        scoreCountText.text = scoreCount.ToString();
        isReadyToShoot = true;
        isFire = false;
    }
    private void Update()
    {
        scoreCountText.text = scoreCount.ToString();

        float xAxis = Input.GetAxis("Horizontal");

        Vector3 pos = transform.position;
        pos.x += xAxis * speed * Time.deltaTime;
        transform.position = pos;

        if (Input.GetKey(KeyCode.Space))
        {
            isFire = true;
        }
        else
        {
            isFire = false;
        }
        transform.rotation = Quaternion.Euler(0, xAxis * rollMult, 0);

        if (isFire && isReadyToShoot)
        {
            Shoot();
        }

        if(heroHP <= 0)
        {
            Destroy(gameObject);
            restartMenuUI.SetActive(true);
            Time.timeScale = 0.00001f;
        }
    }


    void ChangeLife()
    {
        for (int i = 0; i < lifePoints.Length; i++)
        {
            if(i < heroHP)
            {
                lifePoints[i].color = lifeColors[1];
            }
            else
            {
                lifePoints[i].color = lifeColors[0];
            }
        }
    }

    public void AddScore(int scoreToAdd)
    {
        scoreCount += scoreToAdd;
        if (scoreCount > HighScore.score)
        {
            HighScore.score = scoreCount;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Transform rootT = other.gameObject.transform.root;
        GameObject go = rootT.gameObject;

        if(go == lastTriggerGo)
        {
            return;
        }

        lastTriggerGo = go;

        if(go.tag == "Enemy")
        {
            Destroy(go);
        }
    }

    public void Shoot()
    {
        foreach (Transform sp in shootPoints)
        {
            GameObject bulletHero = Instantiate(projectilePferab, sp.position, Quaternion.identity) as GameObject;
            if(sp == shootPoints[shootPoints.Length -1])
            {
                StartCoroutine(ShootDelay());
            }
        }     
    }
    IEnumerator ShootDelay()
    {
        isReadyToShoot = false;
        yield return new WaitForSeconds(shootDelay);
        isReadyToShoot = true;
    }

    public void Damage(int dmg)
    {
        heroHP -= dmg;
        if (heroHP < 0)
        {
            heroHP = 0;
        }
        ChangeLife();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ProjectileEnemy"))
        {
            Damage(1);
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Damage(1);
            Destroy(collision.gameObject);
            AddScore(100);
        }
    }
}
